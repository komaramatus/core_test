#include <Arduino.h>
#include "sys/alt_flash.h"
#include "sys/alt_alarm.h"
#include <string.h>
#include "altera_avalon_pio_regs.h" //IOWR_ALTERA_AVALON_PIO_DATA
#include "sys/alt_alarm.h"
#include "system.h"


#define BUFFER_SIZE 50




bool bPass;

bool TMEM_QuickVerify(alt_u32 BaseAddr, alt_u32 DataSize, alt_u32 DataWidth, alt_u32 AddrWidth){
    bool bPass = true;
    const alt_u32 TestNum = 10;
    const alt_u32 TestPattern = 0xAA;
    alt_u32 mask, Read32, Addr32, TestData32, TestAddr32;
    int i;
    
    // test address line
    mask = 0x01;
    for(i=0;i<AddrWidth && bPass;i++){

        IOWR(BaseAddr, mask, TestPattern);
        Read32 = IORD(BaseAddr, mask);
        if (Read32 != TestPattern)
            bPass = false;
        mask <<= 1;
    }
    
    // test data line
    mask = 0x01;
    for(i=0;i<DataWidth && bPass;i++){
        Addr32 = i*13;
        IOWR(BaseAddr, Addr32, mask);
        Read32 = IORD(BaseAddr, Addr32);
        if (Read32 != mask)
            bPass = false;
        mask <<= 1;
        if (mask == 0x00)
            mask = 0x01;
    }
    
    return bPass;
}

byte* buffer;

double SENSOR_read()
{
    
    uint16_t total =0;
    

    
    for(int i=0;i<BUFFER_SIZE;i++)
    {
        buffer[i] = (byte)IORD_ALTERA_AVALON_PIO_DATA(PIO_0_BASE);
        total += buffer[i];
    }
    
    

    return (double)total / (double)BUFFER_SIZE ;
    
}


float stdev(float mean)
{
    float deviation = 0;
    float sumsqr = 0;
    
    for (int i = 1 ; i< BUFFER_SIZE; i++){
        deviation = buffer[i] - mean;
        sumsqr += deviation * deviation;
    }
    float variance = sumsqr/(float)BUFFER_SIZE ;
    return sqrt(variance) ;
    
}



void setup() {

    Serial.begin(9600);
    // type com6: >> data.log
    Serial0.begin(9600);
    char s[80];
    sprintf(s , "===== SDRAM Test! Size: %dMB (CPU Clock: %dKHz) =====\r\n", SDRAM_CONTROLLER_0_SPAN/1024/1024, ALT_CPU_CPU_FREQ/1000);
    Serial.println(s);
    bPass = TMEM_QuickVerify(SDRAM_CONTROLLER_0_BASE,1024,6,6);
    if (bPass)buffer =  (byte*) malloc(BUFFER_SIZE * sizeof(byte));

}

int itetation =0;

void loop() {
    
    if(!bPass)
    {
        Serial.println("SDRAM test failed! Exiting...");
        while(1);
    }
    
   // comments
    

    char val[30];
    float mean = SENSOR_read();
    sprintf(val , "%.4f^%.4f^%.4f", mean ,stdev(mean), (float)alt_nticks()/(float)alt_ticks_per_second());
    Serial0.println(val);
    
    
    free(buffer);
    itetation++;
}